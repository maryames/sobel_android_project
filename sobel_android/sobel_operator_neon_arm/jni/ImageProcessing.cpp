/*
*  ImageProcessing.cpp
*
*  Authors:Marijana Vujovic
*/
#include <jni.h>

#include <android/log.h>
#include <stdlib.h>
#include <time.h>
#include <arm_neon.h>

#define  LOG_TAG    "libImageProcessing"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

using namespace std;

#define N 8
void yuv420_to_rgb(unsigned char y[16][16], unsigned char u[8][8], unsigned char v[8][8], unsigned char rgb[16][64]);
void rgb_to_y(unsigned char rgb[16][64], unsigned char y[16][16]);
void y_to_rgb(unsigned char y[16][16], unsigned char rgb[16][64]);
void filter_edge_enhance_horz(unsigned char y_buff12x12[12][12], unsigned char y_buff8x8[8][8]);

void convert_yuv420_to_rgb(unsigned char * poutPixels);

void convert_rgb_to_y(unsigned char *y_pic);
void convert_y_to_rgb(unsigned char * outPixels);
void extend_borders();
void perform_filtering();

int XSIZE, YSIZE;
unsigned char *rgb_pic;
unsigned char *y_pic;
unsigned char *y_pic2;
unsigned char y_buff8x8[8][8];
unsigned char y_buff8x8_2[8][8];
unsigned char y_buff12x12[12][12];
unsigned char rgb_buff[16][64];
unsigned char y_buff[16][16];

double now_ms(void) {

    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return 1000.0 * (long)res.tv_sec + (long)res.tv_nsec /1000000.0;

}

extern "C" {void filter_edge_enhance_vert(unsigned char y_buff12x12[12][12], unsigned char y_buff8x8[8][8]);}

extern "C"
jdouble
Java_com_nastava_realtimeimageprocessing_CameraPreview_imageProcessing(JNIEnv* env, jobject thiz,jint width, jint height,jbyteArray NV21FrameData, jintArray outPixels)
{
	jbyte * pNV21FrameData = env->GetByteArrayElements(NV21FrameData, 0);
	jint * poutPixels = env->GetIntArrayElements(outPixels, 0);
	int i;
	int img_size = width*height;
	double timestep;

	XSIZE = width;
	YSIZE = height;
	y_pic = (unsigned char *)malloc((XSIZE+16)*(YSIZE+16));
	y_pic2 = (unsigned char *)malloc((XSIZE+16)*(YSIZE+16));

	for (i=0; i<YSIZE; i++)
	{
		memcpy(&y_pic[(i+8)*(XSIZE+16)+8], &((unsigned char *)pNV21FrameData)[i*XSIZE], XSIZE);
	}

	extend_borders();
	
	// Profiling
	timestep = -now_ms();
	perform_filtering();
	timestep+= now_ms();
	

	convert_y_to_rgb((unsigned char *)poutPixels);

	free(rgb_pic);
	free(y_pic);
	free(y_pic2);

	env->ReleaseByteArrayElements(NV21FrameData, pNV21FrameData, 0);
	env->ReleaseIntArrayElements(outPixels, poutPixels, 0);

	return timestep;
}

void convert_yuv420_to_rgb(unsigned char * poutPixels)
{
  int x, y, i, j;

  for (y=0; y<YSIZE/16; y++)
  {
    for (x=0; x<XSIZE/16; x++)
    {
      for (j=0; j<8; j++)
      {
        for (i=0; i<8; i++)
        {
          y_buff[2*j  ][2*i  ] = y_pic2[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+8];
          y_buff[2*j  ][2*i+1] = y_pic2[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+9];
          y_buff[2*j+1][2*i  ] = y_pic2[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+8];
          y_buff[2*j+1][2*i+1] = y_pic2[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+9];
        }
      }
      y_to_rgb(y_buff,rgb_buff);

      for (j=0; j<16; j++)
      {
        for (i=0; i<64; i++)
        {
        	((unsigned char *)poutPixels)[(16*y+j)*4*XSIZE+64*x+i] = rgb_buff[j][i];
        }
      }
    }
  }
}

void convert_rgb_to_y(unsigned char *y_pic)
{
  int x, y, i, j;

  for (y=0; y<YSIZE/16; y++)
  {
    for (x=0; x<XSIZE/16; x++)
    {
      for (j=0; j<16; j++)
      {
        for (i=0; i<64; i++)
        {
          rgb_buff[j][i] = rgb_pic[(16*y+j)*3*XSIZE+64*x+i];
        }
      }

      rgb_to_y(rgb_buff, y_buff);

      for (j=0; j<8; j++)
      {
        for (i=0; i<8; i++)
        {
          y_pic[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+8] = y_buff[2*j][2*i];
          y_pic[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+9] = y_buff[2*j][2*i+1];
          y_pic[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+8] = y_buff[2*j+1][2*i];
          y_pic[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+9] = y_buff[2*j+1][2*i+1];
        }
      }
    }
  }
}

void convert_y_to_rgb(unsigned char* outPixels)
{
  int x, y, i, j;

  for (y=0; y<YSIZE/16; y++)
  {
    for (x=0; x<XSIZE/16; x++)
    {
      for (j=0; j<8; j++)
      {
        for (i=0; i<8; i++)
        {
          y_buff[2*j  ][2*i  ] = y_pic2[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+8];
          y_buff[2*j  ][2*i+1] = y_pic2[(16*y+2*j+8)*(XSIZE+16)+16*x+2*i+9];
          y_buff[2*j+1][2*i  ] = y_pic2[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+8];
          y_buff[2*j+1][2*i+1] = y_pic2[(16*y+2*j+9)*(XSIZE+16)+16*x+2*i+9];
        }
      }

      y_to_rgb(y_buff, rgb_buff);


      for (j=0; j<16; j++)
      {
        for (i=0; i<64; i++)
        {
        	(outPixels)[(16*y+j)*4*XSIZE+64*x+i] = rgb_buff[j][i];
        }
      }

    }
  }
}
void extend_borders()
{
  int i;

  for (i=0; i<8; i++)
  {
    memcpy(&y_pic[i*(XSIZE+16)+8], &y_pic[8*(XSIZE+16)+8], XSIZE);
    memcpy(&y_pic[(YSIZE+8+i)*(XSIZE+16)+8], &y_pic[(YSIZE+7)*(XSIZE+16)+8], XSIZE);
  }

  for (i=0; i<YSIZE+16; i++)
  {
    memset(&y_pic[i*(XSIZE+16)], y_pic[i*(XSIZE+16)+8], 8);
    memset(&y_pic[i*(XSIZE+16)+XSIZE+8], y_pic[i*(XSIZE+16)+XSIZE+7], 8);
  }
}

void perform_filtering()
{
	  int x, y, i, j;

	  for (y=0; y<YSIZE/8; y++)
	  {
	    for (x=0; x<XSIZE/8; x++)
	    {
	      for (j=0; j<12; j++)
	      {
	        for (i=0; i<12; i++)
	        {
	          y_buff12x12[j][i] = y_pic[(8*y+j+6)*(XSIZE+16)+8*x+i+6];

	        }
	      }

	      filter_edge_enhance_horz(y_buff12x12, y_buff8x8);
	      filter_edge_enhance_vert(y_buff12x12, y_buff8x8_2);

	      for (j=0; j<8; j++)
	      {
	        for (i=0; i<8; i++)
	        {
	          if (y_buff8x8[j][i] < y_buff8x8_2[j][i])
	            y_pic2[(8*y+j+8)*(XSIZE+16)+8*x+i+8] = y_buff8x8[j][i];
	          else
	            y_pic2[(8*y+j+8)*(XSIZE+16)+8*x+i+8] = y_buff8x8_2[j][i];
	        }
	      }
	    }
	  }
}
//prepravila 64
void yuv420_to_rgb(unsigned char y[16][16], unsigned char u[8][8], unsigned char v[8][8], unsigned char rgb[16][64])
{
  int i, j, Y, U, V, UV, r, g, b;

  for (j=0; j<8; j++)
  {
    for (i=0; i<8; i++)
    {
      U = (int)u[j][i] - 128;
      V = (int)v[j][i] - 128;

      UV = -6657 * U - 13424 * V;
      U = 33311 * U;
      V = 26355 * V;

      Y = 19077 * (y[2*j][2*i] - 16);

      r = (Y + V) >> 14;
      if (r < 0)
        r = 0;
      else if (r > 255)
        r = 255;
      g = (Y + UV) >> 14;
      if (g < 0)
        g = 0;
      else if (g > 255)
        g = 255;
      b = (Y + U) >> 14;
      if (b < 0)
        b = 0;
      else if (b > 255)
        b = 255;

      rgb[2*j][8*i  ] = b;
      rgb[2*j][8*i+1] = g;
      rgb[2*j][8*i+2] = r;
      rgb[2*j][8*i+3] = 0xFF;

      Y = 19077 * (y[2*j][2*i+1] - 16);

      r = (Y + V) >> 14;
      if (r < 0)
        r = 0;
      else if (r > 255)
        r = 255;
      g = (Y + UV) >> 14;
      if (g < 0)
        g = 0;
      else if (g > 255)
        g = 255;
      b = (Y + U) >> 14;
      if (b < 0)
        b = 0;
      else if (b > 255)
        b = 255;

      rgb[2*j][8*i+4] = b;
      rgb[2*j][8*i+5] = g;
      rgb[2*j][8*i+6] = r;
      rgb[2*j][8*i+7] = 0xFF;

      Y = 19077 * (y[2*j+1][2*i] - 16);

      r = (Y + V) >> 14;
      if (r < 0)
        r = 0;
      else if (r > 255)
        r = 255;
      g = (Y + UV) >> 14;
      if (g < 0)
        g = 0;
      else if (g > 255)
        g = 255;
      b = (Y + U) >> 14;
      if (b < 0)
        b = 0;
      else if (b > 255)
        b = 255;

      rgb[2*j+1][8*i  ] = b;
      rgb[2*j+1][8*i+1] = g;
      rgb[2*j+1][8*i+2] = r;
      rgb[2*j+1][8*i+3] = 0xFF;

      Y = 19077 * (y[2*j+1][2*i+1] - 16);

      r = (Y + V) >> 14;
      if (r < 0)
        r = 0;
      else if (r > 255)
        r = 255;
      g = (Y + UV) >> 14;
      if (g < 0)
        g = 0;
      else if (g > 255)
        g = 255;
      b = (Y + U) >> 14;
      if (b < 0)
        b = 0;
      else if (b > 255)
        b = 255;

      rgb[2*j+1][8*i+4] = b;
      rgb[2*j+1][8*i+5] = g;
      rgb[2*j+1][8*i+6] = r;
      rgb[2*j+1][8*i+7] = 0xFF;
    }
  }
}

void rgb_to_y(unsigned char rgb[16][64], unsigned char y[16][16])
{
  int i, j;
  int Y;

  for (j=0; j<16; j++)
  {
    for (i=0; i<16; i++)
    {
       Y = 66 * rgb[j][3*i+2] + 129 * rgb[j][3*i+1] + 25 * rgb[j][3*i];
       Y = (Y + 128) >> 8;
       y[j][i] = Y + 16;
    }
  }
}

void y_to_rgb(unsigned char y[16][16], unsigned char rgb[16][64])
{
  int i, j, Y;

  for (j=0; j<16; j++)
  {
    for (i=0; i<16; i++)
    {
      Y = 149 * (y[j][i] - 16);
      Y = Y >> 7;
      if (Y < 0)
        Y = 0;
      else if (Y > 255)
        Y = 255;

      rgb[j][4*i  ] = Y;
      rgb[j][4*i+1] = Y;
      rgb[j][4*i+2] = Y;
      rgb[j][4*i+3] = 0xFF;
    }
  }
}

short sobel_filter_h[3][3] =
{
  { -1, -2, -1 },
  {  0,  0,  0 },
  {  1,  2,  1 }
};

short sobel_filter_v[3][3] =
{
  { -1,  0,  1 },
  { -2,  0,  2 },
  { -1,  0,  1 }
};

void filter_edge_enhance_horz(unsigned char y_buff12x12[12][12], unsigned char y_buff8x8[8][8])
{
  int i, j, k, l;

  for (j=0; j<8; j++)
  {
    for (i=0; i<8; i++)
    {
      short sample;

      sample = 0;
      for (k=0; k<3; k++)
        for (l=0; l<3; l++)
          sample += sobel_filter_h[k][l] * y_buff12x12[j+k+1][i+l+1];
      sample = (sample + 1) >> 1;
      if (sample < 0)
        sample = -sample;
      if (sample > 255)
        sample = 255;
      y_buff8x8[j][i] = 255-(unsigned char)sample;
    }
  }
}

