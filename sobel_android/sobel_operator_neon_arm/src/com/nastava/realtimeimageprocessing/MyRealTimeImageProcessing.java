package com.nastava.realtimeimageprocessing;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class MyRealTimeImageProcessing extends Activity 
{
	private CameraPreview camPreview;
	private ImageView myCameraPreview = null;
	private FrameLayout mainLayout;
	private TextView fpsView;
	private int previewSizeWidth;
 	private int previewSizeHeight;
 	
 	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        //Set this APK Full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
				 			WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Set this APK no title
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        setContentView(R.layout.main);
        Display display = getWindowManager().getDefaultDisplay(); 
        previewSizeWidth = display.getWidth();
        previewSizeHeight = display.getHeight();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  
        //
        // Create my camera preview 
        //
        myCameraPreview = new ImageView(this);

      	fpsView = (TextView)findViewById(R.id.textView1);
        SurfaceView camView = new SurfaceView(this);
        SurfaceHolder camHolder = camView.getHolder();
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        camPreview = new CameraPreview(previewSizeWidth, previewSizeHeight, myCameraPreview, fpsView);
        
        camHolder.addCallback(camPreview);
        
        mainLayout = (FrameLayout) findViewById(R.id.frameLayout1);
        mainLayout.addView(camView, new LayoutParams(previewSizeWidth, previewSizeHeight));
        mainLayout.addView(myCameraPreview, new LayoutParams(previewSizeWidth, previewSizeHeight));
        mainLayout.bringChildToFront(myCameraPreview);
    }

	@Override
    protected void onPause()
	{
		if ( camPreview != null)
			camPreview.onPause();
		super.onPause();
	}
}