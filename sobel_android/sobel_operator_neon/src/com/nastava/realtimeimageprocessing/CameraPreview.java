/*
*  CameraPreview.java
*/
package com.nastava.realtimeimageprocessing;

import java.io.IOException;
import java.util.List;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.ImageView;
import android.widget.TextView;

import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;

public class CameraPreview implements SurfaceHolder.Callback, Camera.PreviewCallback
{
	private Camera camera = null;
	private ImageView myCameraPreview = null;
	private Bitmap bitmap = null;
	private int[] pixels = null;
	private byte[] frameData = null;
	private int imageFormat;
	private int previewSizeWidth;
 	private int previewSizeHeight;
 	private boolean isProcessing = false;
 	private TextView myFpsView = null;
 	private double elapsedTime = 0, elapsedTime_old, procTime = 0;
 	private short i = 1;
 	
 	Handler mHandler = new Handler(Looper.getMainLooper());
 	
	public CameraPreview(int previewlayoutWidth, int previewlayoutHeight,
    		ImageView cameraPreview, TextView fpsView)
    {
		previewSizeWidth = previewlayoutWidth;
    	previewSizeHeight = previewlayoutHeight;
    	myCameraPreview = cameraPreview;
    	myCameraPreview.setScaleType(ImageView.ScaleType.FIT_XY);
    	bitmap = Bitmap.createBitmap(previewSizeWidth, previewSizeHeight, Bitmap.Config.ARGB_8888);
    	pixels = new int[previewSizeWidth * previewSizeHeight];
    	myFpsView = fpsView;
    	elapsedTime_old = SystemClock.elapsedRealtime();
    }

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) 
	{
		// At preview mode, the frame data will push to here.
		if (imageFormat == ImageFormat.NV21)
        {
			//We only accept the NV21(YUV420) format.
			if ( !isProcessing )
			{
				frameData = data;
				if ( (i%10)==0){
					elapsedTime = SystemClock.elapsedRealtime();
					myFpsView.setText("FPS: " + Integer.toString(10000/(int)(elapsedTime-elapsedTime_old)) + "  AVT: " 
							+ Double.toString(procTime/i).substring(0, 6) + " ms");
					elapsedTime_old = elapsedTime;
				} 
				mHandler.post(DoImageProcessing);
				i++;
			}
        }
	}
	
	public void onPause()
    {
    	camera.stopPreview();
    }

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) 
	{
	    Parameters parameters;
		
	    parameters = camera.getParameters();
	    // Set the camera preview size
		List<Size> supportedSize = parameters.getSupportedPreviewSizes();
		Size camSize = camera.new Size(previewSizeWidth, previewSizeHeight);
	    if(!supportedSize.contains(camSize)){
	    	previewSizeWidth = 0;
	    	previewSizeHeight = 0;
	    	for(Size s:supportedSize){
				if(s.width >= previewSizeWidth && s.height>=previewSizeHeight){
					previewSizeWidth = s.width;
					previewSizeHeight = s.height;
				}
			}
	    }
	    parameters.setPreviewSize(previewSizeWidth, previewSizeHeight);
	    bitmap = Bitmap.createBitmap(previewSizeWidth, previewSizeHeight, Bitmap.Config.ARGB_8888);
	    
		imageFormat = parameters.getPreviewFormat();
		camera.setParameters(parameters);
		camera.startPreview();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) 
	{
		camera = Camera.open();
		//if there is no back camera on device
		//mCamera = Camera.opne(0);
		try
		{
			// If did not set the SurfaceHolder, the preview area will be black.
			camera.setPreviewDisplay(holder);
			camera.setPreviewCallback(this);
		} 
		catch (IOException e)
		{
			camera.release();
			camera = null;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) 
	{
    	camera.setPreviewCallback(null);
		camera.stopPreview();
		camera.release();
		camera = null;
	}

	//
	// Native JNI 
	//
    public native double imageProcessing(int width, int height, 
    		byte[] NV21FrameData, int [] pixels);
    static 
    {
        System.loadLibrary("ImageProcessing");
    }
    
    private Runnable DoImageProcessing = new Runnable() 
    {
        public void run() 
        {
    		Log.i("MyRealTimeImageProcessing", "DoImageProcessing():");
        	isProcessing = true;
        	procTime += imageProcessing(previewSizeWidth, previewSizeHeight, frameData, pixels );
			
			bitmap.setPixels(pixels, 0, previewSizeWidth, 0, 0, previewSizeWidth, previewSizeHeight);
			myCameraPreview.setImageBitmap(bitmap);
			isProcessing = false;
        }
    };
   }
